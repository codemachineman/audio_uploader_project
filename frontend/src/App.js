import React, { Component } from 'react';
import axios from 'axios';
import logo from './logo.svg';
import './App.css';

class App extends Component {

  constructor(props){
      super(props);
      this.fileChangedHandler = this.fileChangedHandler.bind(this);
      this.uploadHandler = this.uploadHandler.bind(this);
      this.state = {
        audioFile: null,
        fileChosen: false,
        audioUploaded: false,
        audioLink: '',
      };
    }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to Audio Uploader</h1>
        </header>
        <p className="App-intro">
          {this.headerText()}
          <br></br>
          <br></br>
          {this.fileInput()}
          <br></br>
          {this.uploadButton()}
        </p>
        {this.audioPlayer()}
      </div>
    );
  }

  uploadHandler(){
    console.log(this.state.audioFile)
    const form = new FormData();
    form.append('audio',this.state.audioFile)
    form.append('name', this.state.audioFile.name)
    axios.post('http://localhost:8000/api/songs/',form)
    .then(result =>
      {
        console.log(result)
        var data = result.data
        console.log(data.audio)
      }).catch(e => {
    console.log(e);
    });
    this.setState({audioUploaded: true}, () => console.log(this.state));
  }

  fileChangedHandler (event) {
    event.preventDefault();
    const file = event.target.files[0];
    console.log('we have triggered the file event!');
    console.log('filename: ' + event.target.files[0].name);
    const data = new FormData();
    this.setState({audioFile: event.target.files[0]}, () => console.log(this.state));
    this.setState({fileChosen: true}, () => console.log(this.state));
  }

  audioPlayer()
  {
    if(this.state.audioUploaded)
      return (<audio ref="audio_tag" src={this.state.audioLink}  controls/>);
    return ('')
  }

  uploadButton()
  {
    if(this.state.fileChosen && !this.state.audioUploaded)
      return <button onClick={this.uploadHandler}>Upload!</button>
    return ''
  }

  headerText()
  {
    if(this.state.audioUploaded)
      return this.state.audioFile.name
    return 'Select a file you would like to upload...'
  }

  fileInput()
  {
    if(this.state.audioUploaded)
      return ''
    return <input type="file"
    accept=".mp3"
    onChange={this.fileChangedHandler}
    ></input>
  }
}

export default App;
