# Audio Uploader

This project is a demo of a Django/React application that allows a user to upload
a file audio file to the server and then listen to it in an HTML5 audio player.

The Django server runs on port 8000 and handles the back end buiness logic, while the react app, which listens on port 3000 handle the UI as the front end plugin.

The assets usded for this demo are as follows
- [React](https://facebook.github.io/react/), for building interactive UIs
- [Webpack](https://webpack.js.org/), for bundling static assets
- [virtualenvwrapper-win](http://virtualenvwrapper.readthedocs.io/en/latest/install.html), for creating virtual shell enviroments
- [create-react-app](https://github.com/facebook/create-react-app), for creating react apps
- [npm](https://www.npmjs.com/), for installing and updating tools
- [sqlite3](https://www.sqlite.org/index.html), for as a development db
- [docker](https://www.docker.com/), for running the app in a docker container

### Deployment (no docker)

To start the web app run the following commands, from the project's root directory:

install virtual enviroment wrapper

`pip install virtualenvwrapper-win`

create your virtual enviroment

`mkvirtualenv audio_uploader`

if you have already created your virtual enviroment and you need to re-enter it

`workon audio_upload`

perform db migrations

`manage.py migrate`

install the correct version of npm (to prevent the version 5+ bug)

`npm install -g npm@4.6.1`

install react native scrips

`npm install react-native-scripts`

#### Launching the app

While inside the audio_uploader virtual enviroment go to the project root and run the following command:

`manage.py runserver 8000`

This is start the Django webserver which has been configured to use the react app as it's front end.

Now go to `project_root/frontend` and run the following command:

`npm run start`

This will start the react front end and you should now be able to see the react logo on the web page at `localhost:8000`

### Deployment (docker)

Dowload and install docker. Go the project root and where the Dockerfile is located and run the following command:

`docker build -t <image> .`

once the image has been created you can run it in docker:

`docker run <image>`

The Dockerfile should lauch react and django and map the port appropriately.

##### Known Issues

Static files in subfolders of the STATIC_URL are not served to clients
- Audio files that have been uploaded to the django server's static folder can not be accessed by the audio player

  Solution: Switch to S3 file hosting on aws so django doesn't have to serve static files. File urls from S3 will be stored in Filefield of the song model.

NPM could not be installed on docker
- required to start the react frontend

  possible solution: install nodejs package

manage.py is 'not found' when attemping to run from bash with `docker run -it <image> sh`

```
PS C:\Users\Admin\Documents\audio_uploader\audio_uploader> docker run -it audio sh
# ls
Dockerfile  audio_uploader  db.sqlite3  debug.log  frontend  manage.py  requirements.txt  songs  templates  webpack-stats.dev.json
# manage.py runserver
sh: 2: manage.py: not found
```

Middleware removal to prevent POST data from being blocked by cross site request forgery protection when rest test client
- removed the middleware django.middleware.csrf.CsrfViewMiddleware from the django app to bypass security barriers

Note: be sure to add the csrf protection back in before going to production
